#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

weekday=`date +%a`
if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_user=cvsft-nightlies
else
  cvmfs_user=cvsft
fi
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch
for iterations in {1..10}
do
  if [[ "${BUILDMODE}" == "nightly" ]]; then
      cvmfs_server transaction sft-nightlies.cern.ch
  else
      cvmfs_server transaction sft.cern.ch
  fi
  if [ "\$?" == "1" ]; then
    if  [[ "\$iterations" == "10" ]]; then
      echo "Too many tries... "
      # After 10 tries in nightly mode the opened transaction is forcibly aborted
      # Only during the night ( between 7pm and 8am)
      H=`date +%H`
      if [[ "${BUILDMODE}" == "nightly" && (19 -le "$H" || "$H" -lt 8) ]]; then
          echo "Forcing transaction abortion"
          cvmfs_server abort -f sft-nightlies.cern.ch
          cvmfs_server transaction sft-nightlies.cern.ch
      else
          exit 1
      fi
    else
       echo "Transaction is already open. Going to sleep..."
       sleep 10m
    fi
  else
    break
  fi
done

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo Weekday:   $weekday
echo BuildMode: $BUILDMODE
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

set -x

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}

if [[ "${BUILDMODE}" == "nightly" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/$LCG_VERSION/$weekday /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/$LCG_VERSION
  if [ "\$?" != "0" ]; then
    echo "There was an error installing packages, quitting"
    cvmfs_server abort sft-nightlies.cern.ch
    exit 1
  fi
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft-nightlies.cern.ch/lcg/views/$LCG_VERSION/$weekday/$PLATFORM /cvmfs/sft-nightlies.cern.ch/lcg/views/$LCG_VERSION/$weekday
  if [ "\$?" == "0" ]; then
    echo "View created, updating latest symlink"
    rm /cvmfs/sft-nightlies.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM
    cd /cvmfs/sft-nightlies.cern.ch/lcg/views/$LCG_VERSION/latest
    ln -s ../$weekday/$PLATFORM
  else
    echo "There was an error creating view, not updating latest symlink"
  fi
  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/$LCG_VERSION/$weekday $PLATFORM $LCG_VERSION RELEASE
  cd $HOME
  cvmfs_server publish sft-nightlies.cern.ch
elif [[ "${BUILDMODE}" == "release" ]]; then
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/releases /cvmfs/sft.cern.ch/lcg
  cvmfs_rsync -a -e "ssh -o StrictHostKeyChecking=no" --del sftnight@${BUILDHOSTNAME}:/Users/Shared/cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}
  $WORKSPACE/lcgcmake/jenkins/extract_LCG_summary.py /cvmfs/sft.cern.ch/lcg/releases/LCG_${LCG_VERSION} $PLATFORM $LCG_VERSION RELEASE
  cd $HOME
  cvmfs_server publish sft.cern.ch
fi
EOF

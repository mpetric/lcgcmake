cmake_minimum_required(VERSION 3.9 FATAL_ERROR)
project(LCGSoft C CXX Fortran)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/modules ${CMAKE_SOURCE_DIR}/cmake/toolchain ${CMAKE_MODULE_PATH})
if(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE) # Default to a Release build
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build [Release MinSizeRel Debug RelWithDebInfo]" FORCE)
endif()

#---Definition options and teir default values ------------------------------------------------------------------------
set(LCG_SAFE_INSTALL    OFF    CACHE BOOL "Ensure that no overwites occurs at the installation area of packages")
if (VALIDATION)
  set(LCG_IGNORE        "pythia8;pythia6;agile;rivet;herwig++"       CACHE STRING "List of packages to be ignored from LCG_INSTALL_PREFIX (';' separated)")
else()
  set(LCG_IGNORE         ""    CACHE STRING "List of packages to be ignored from LCG_INSTALL_PREFIX (';' separated)")
endif()
set(LCG_INSTALL_PREFIX   ""    CACHE STRING "Existing LCG installation(s) path(s). Can be a list (';' separated)")
set(LCG_ADDITIONAL_REPOS ""    CACHE STRING "Addional binary repositories for tarfiles. Can be a list (';' separated)")
set(LCG_TARBALL_INSTALL  OFF   CACHE BOOL "Turn ON/OFF creation/installation of tarballs")
set(LCG_SOURCE_INSTALL   OFF   CACHE BOOL "Turn ON/OFF installation of sources in /share")
set(VALIDATION           OFF   CACHE BOOL "Enable validation settings.")
set(POST_INSTALL         ON    CACHE BOOL "Enable validation settings.")
set(STRIP_RPATH          ON    CACHE BOOL "Strip RPATH from binaries.")
set(USE_BINARIES         OFF   CACHE BOOL "Download binaries if they exists in repository(s) (changes installation layout).")
set(LCG_BUILD_ALWAYS     OFF   CACHE BOOL "Force the 'build' step for all projects that are locally built (not downloaded nor symlinked).")
string(REPLACE " " ";" LCG_IGNORE "${LCG_IGNORE}")
string(REPLACE ":" ";" LCG_INSTALL_PREFIX "${LCG_INSTALL_PREFIX}")

#---Report the values of the options-----------------------------------------------------------------------------------
message(STATUS "Target installation prefix             : ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Existing LCG installations             : ${LCG_INSTALL_PREFIX}")
message(STATUS "Additional binary repositories         : ${LCG_ADDITIONAL_REPOS}")
message(STATUS "Safe installation                      : ${LCG_SAFE_INSTALL}")
message(STATUS "Source and binary tarball installation : ${LCG_TARBALL_INSTALL}")
message(STATUS "Source installation in /share          : ${LCG_SOURCE_INSTALL}")
message(STATUS "Validation mode                        : ${VALIDATION}")
message(STATUS "Preparing .post-install.sh scripts     : ${POST_INSTALL}")
message(STATUS "Stripping RPATH from binaries          : ${STRIP_RPATH}")
message(STATUS "Download binaries if they exists       : ${USE_BINARIES}")
message(STATUS "Ignored packages from LCG installation : ${LCG_IGNORE}")
message(STATUS "Python major version in LCG stack      : ${LCG_PYTHON_VERSION}")
message(STATUS "Force build of all local projects      : ${LCG_BUILD_ALWAYS}")

set(GenURL https://lcgpackages.web.cern.ch/tarFiles/sources)
set(BinURL https://lcgpackages.web.cern.ch/tarFiles/releases)

include(${CMAKE_SOURCE_DIR}/toolchain.cmake)
include(lcgsoft-macros)

#---Define Global variables--------------------------------------------------------------------------------------------
find_program(env_cmd NAMES env)
mark_as_advanced(env_cmd)
set(EXEC ${env_cmd})
set(MAKE ${CMAKE_SOURCE_DIR}/cmake/scripts/make_verbose $(MAKE))
set(LOCKFILE ${CMAKE_SOURCE_DIR}/cmake/scripts/lockfile.py)
set(PYTHON python${Python_version_major})

#--- Select the vector instruction set --------------------------------------------------------------------------------
set(LCG_INSTRUCTIONSET  "" CACHE STRING "Vector instruction sets (+ separated)")
string(REPLACE "+" ";" LCG_INSTRUCTIONSET "${LCG_INSTRUCTIONSET}")
message (STATUS "LCG_INSTRUCTIONSET                     : [${LCG_INSTRUCTIONSET}]")

string(TOUPPER "${CMAKE_BUILD_TYPE}" _build_type_upper)
if (_build_type_upper)
  set (_flag_suffix "_${_build_type_upper}")
endif()
message (STATUS "LCG_CPP11                              : ${LCG_CPP11}")
message (STATUS "LCG_CPP1Y                              : ${LCG_CPP1Y}")
message (STATUS "LCG_CPP14                              : ${LCG_CPP14}")
message (STATUS "LCG_CPP17                              : ${LCG_CPP17}")

if(LCG_CPP14)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++14)
  set(CMAKE_CXX_STANDARD 14)
elseif(LCG_CPP1Y)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++1y)
  set(CMAKE_CXX_STANDARD 11)  # CMake does not understand 1Y as standard
elseif(LCG_CPP11)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++11)
  set(CMAKE_CXX_STANDARD 11)
elseif(LCG_CPP17)
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++17)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(DEFINED cuda_native_version)
  if(${cuda_native_version} VERSION_GREATER 10)
    set(CMAKE_CUDA_STANDARD 14)
  elseif(${cuda_native_version} VERSION_GREATER 7)
    set(CMAKE_CUDA_STANDARD 11)
  endif()
endif()

# Set MacOS target-----------------------------------------------------------------------------------------------------
if(APPLE)
  execute_process(COMMAND sw_vers "-productVersion"
                  COMMAND cut -d . -f 1-2
                  OUTPUT_VARIABLE osvers OUTPUT_STRIP_TRAILING_WHITESPACE)
  set(CMAKE_OSX_DEPLOYMENT_TARGET ${osvers})
endif()

# Setup the compiler wrapper support different instruction sets
if(LCG_INSTRUCTIONSET)
  get_filename_component(C_NAME ${CMAKE_C_COMPILER} NAME)
  get_filename_component(CXX_NAME ${CMAKE_CXX_COMPILER} NAME)
  get_filename_component(FC_NAME ${CMAKE_Fortran_COMPILER} NAME)
  foreach(iset ${LCG_INSTRUCTIONSET})
    set(WRAPPER_FLAGS "${WRAPPER_FLAGS} -m${iset}")
    set(LCG_ISET "${LCG_ISET}+${iset}")
  endforeach()
  message(STATUS "Instruction set flags                  :${WRAPPER_FLAGS}")
  configure_file(cmake/scripts/c_wrapper.in compilers/${C_NAME} @ONLY)
  configure_file(cmake/scripts/cxx_wrapper.in compilers/${CXX_NAME} @ONLY)
  configure_file(cmake/scripts/fc_wrapper.in compilers/${FC_NAME} @ONLY)
  set(CMAKE_C_WRAPPER ${CMAKE_BINARY_DIR}/compilers/${C_NAME})
  set(CMAKE_CXX_WRAPPER ${CMAKE_BINARY_DIR}/compilers/${CXX_NAME})
  set(CMAKE_Fortran_WRAPPER ${CMAKE_BINARY_DIR}/compilers/${FC_NAME})
  set(LCG_naked_system ${LCG_system})
  string(REPLACE ${LCG_ARCH} ${LCG_ARCH}${LCG_ISET} LCG_system ${LCG_system})
else()
  set(CMAKE_C_WRAPPER ${CMAKE_C_COMPILER})
  set(CMAKE_CXX_WRAPPER ${CMAKE_CXX_COMPILER})
  set(CMAKE_Fortran_WRAPPER ${CMAKE_Fortran_COMPILER})
  set(LCG_naked_system ${LCG_system})
endif()
message(STATUS "LCG_system                             : ${LCG_system}")
message(STATUS "LCG_naked_system                       : ${LCG_naked_system}")

#---Load the packages to be taken from the system----------------------------------------------------------------------
include(${CMAKE_SOURCE_DIR}/cmake/toolchain/systemtools.cmake OPTIONAL)
message(STATUS "Packages taken from the system         : ${LCG_system_packages}")

message (STATUS "Common compiler flags: ")
foreach (_compiler C CXX Fortran)
  message(STATUS "  ${_compiler} : ${CMAKE_${_compiler}_FLAGS} ${CMAKE_${_compiler}_FLAGS${_flag_suffix}}")
endforeach()

# defines LIBRDIR variable to be either lib or lib64
set(LIBDIR_DEFAULT lib)
if(CMAKE_SYSTEM_NAME MATCHES "Linux" AND NOT CMAKE_CROSSCOMPILING AND NOT EXISTS "/etc/debian_version")
  if("${CMAKE_SIZEOF_VOID_P}" EQUAL "8")
    set(LIBDIR_DEFAULT lib64)
  endif()
endif()

# define Fortran RT library
execute_process(COMMAND ${CMAKE_Fortran_COMPILER} -print-file-name=libgfortran${CMAKE_SHARED_LIBRARY_SUFFIX}
                        OUTPUT_VARIABLE FORTRAN_LIBRARY
                        OUTPUT_STRIP_TRAILING_WHITESPACE)
get_filename_component(FORTRAN_LIBRARY_DIR ${FORTRAN_LIBRARY} DIRECTORY)

#---Enable (the defintion) of tests by default-------------------------------------------------------------------------
enable_testing()
include(CTest)

set(TESTLOGDIR "${PROJECT_BINARY_DIR}/tests")
if(NOT "$ENV{CXXFLAGS}" STREQUAL "")
  set(libtoolpatch CXX=$ENV{CXX}\ $ENV{CXXFLAGS})
endif()

file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/timestamps)

set(MakeSitePackagesDir ${CMAKE_COMMAND} -E  make_directory <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages)

add_subdirectory(contrib)
add_subdirectory(externals)
add_subdirectory(gridexternals)
add_subdirectory(projects)
add_subdirectory(pyexternals)
add_subdirectory(generators)
add_subdirectory(frameworks)

#--Add additional [user] recipes---------------------------------------------------------------------------------------
function(define_new_recipes) 
  foreach(name ${LCG_user_recipes})
    LCGPackage_Add(${${name}_recipe})
  endforeach()
endfunction()
define_new_recipes()

if (VALIDATION)
  add_custom_target(validation.pre_builds DEPENDS rivet ROOT lhapdf Python HepMC agile cmaketools)
  add_custom_target(validation.pre_tests COMMAND ctest -R lhapdf6sets.download COMMAND ctest -R rivet-tests.genser-prepare_references COMMAND ctest -R rivet-tests.genser-buildanalysis DEPENDS validation.pre_builds)
  foreach(pkg sherpa pythia8 pythia6 herwig herwig++)
    set (_deplist)
    foreach(v ${${pkg}_native_version})
      add_custom_target(validation.${pkg}-${v} COMMAND ${CMAKE_SOURCE_DIR}/generators/validation/run.sh ${pkg} ${v} DEPENDS ${pkg}-${v} validation.pre_tests)
      list (APPEND _deplist validation.${pkg}-${v})
    endforeach()
    add_custom_target(validation.${pkg} DEPENDS ${_deplist})
    list(APPEND _depglobal validation.${pkg})
  endforeach()
  add_custom_target(validation DEPENDS ${_depglobal})
  add_custom_target(validation.publish COMMAND ${CMAKE_SOURCE_DIR}/generators/validation/publish.sh)
endif()

LCG_create_dependency_files()
file(WRITE ${CMAKE_BINARY_DIR}/fail-logs.txt "")

#---Target to create a LCG view in the install prefix area
add_custom_target(view
                  COMMAND ${CMAKE_ENV} LCG_VERSION=${LCG_VERSION} 
                  ${CMAKE_SOURCE_DIR}/cmake/scripts/create_lcg_view.py
                  -l ${CMAKE_INSTALL_PREFIX} -p ${LCG_system} -d -B ${CMAKE_INSTALL_PREFIX}/${LCG_system})

#---Define the 'default' target
if(DEFINED LCG_targets)
  add_custom_target(top_packages COMMENT "Target to build all top packages for the current stack" DEPENDS ${LCG_targets} )
endif()

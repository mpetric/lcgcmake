#---Base LCG configuration--------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

#---Additional overwrites---------------------------------------------------------
LCG_remove_package(COOL)
LCG_remove_package(CORAL)
LCG_remove_package(EDM4hep)

LCG_external_package(ROOT     v6.22.06)

LCG_external_package(veccore  HEAD  GIT=https://github.com/root-project/veccore.git   )
LCG_external_package(vecmath  HEAD  GIT=https://github.com/root-project/vecmath.git   )
#LCG_external_package(VecGeom  HEAD  GIT=https://gitlab.cern.ch/VecGeom/VecGeom.git    )

LCG_top_packages(ROOT xrootd Geant4 hepmc3 umesimd VecGeom Vc veccore vecmath benchmark ninja CMake lcgenv doxygen pytest)

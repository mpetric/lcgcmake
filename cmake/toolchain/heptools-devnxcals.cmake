#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

LCG_external_package(ROOT    v6.24.00 )


#---Finding latest versions of nxcals packages---------------------
#Initialize token for eos access:
execute_process(COMMAND ${CMAKE_SOURCE_DIR}/jenkins/kinit.sh)

set(EOS_PATH_NXCALS_DEV /eos/project-n/nxcals/swan/nxcals_staging)
set(PATTERN_NXCALS_JAVA "data-access-libs")
set(PATTERN_NXCALS_PYTHON "extraction_api")

execute_process(COMMAND $ENV{SHELL} -c "ls ${EOS_PATH_NXCALS_DEV} | grep ${PATTERN_NXCALS_JAVA} | grep -Eo \'[0-9]+\\.(\\.[0-9]+|[0-9]+)+\'" OUTPUT_VARIABLE NXCALS_JAVA_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls ${EOS_PATH_NXCALS_DEV} | grep ${PATTERN_NXCALS_PYTHON} | grep -Eo \'[0-9]+\\.(\\.[0-9]+|[0-9]+)+\'" OUTPUT_VARIABLE NXCALS_PYTHON_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

#---Finding latest versions of acc-py packages---------------------
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/acc-py --no-deps jpype1")
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/acc-py --no-deps cmmnbuild_dep_manager")
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/acc-py --no-deps pytimber")

execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/acc-py | grep JPype1 | grep -Eo \'[0-9]+\\.(\\.[0-9]+|[0-9]+)+\'" OUTPUT_VARIABLE JPYPE1_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/acc-py | grep cmmnbuild | grep -Eo \'[0-9]+\\.(\\.[0-9]+|[0-9]+)+\'" OUTPUT_VARIABLE CMMNBUILD_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/acc-py | grep pytimber | grep -Eo \'[0-9]+\\.(\\.[0-9]+|[0-9]+)+\'" OUTPUT_VARIABLE PYTIMBER_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

#---Nxcals needs Java11--------------------------------------------
LCG_external_package(java                     11.0.11_9                     )

#---Add nxcals packages--------------------------------------------
LCG_external_package(nxcals_data_access_libs  "${NXCALS_JAVA_VERSION}"      )
LCG_external_package(nxcals_extraction_api    "${NXCALS_PYTHON_VERSION}"    )

#---Add acc-py packages--------------------------------------------
LCG_external_package(jpype1                   "${JPYPE1_VERSION}"                            )
#---Pytimber and cmmnbuild must be build together, since pytimber must create some jar files in the cmnn repo
LCG_external_package(cmmnbuild_pytimber       "${CMMNBUILD_VERSION}-${PYTIMBER_VERSION}"     )

#---Hepak needs a newer version of numpy---------------------------
LCG_external_package(numpy             1.20.3                                )

#---Remove packages not needed by nxcals--------------
LCG_remove_package(CORAL)
LCG_remove_package(COOL)
LCG_remove_package(Garfield++)



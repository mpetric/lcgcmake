#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    HEAD     GIT=http://root.cern.ch/git/root.git         )
LCG_external_package(hepmc3  HEAD     GIT=https://gitlab.cern.ch/hepmc/HepMC3.git  )
LCG_external_package(DD4hep  master   GIT=https://github.com/AIDASoft/DD4hep.git   )
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES fc|ubuntu|mac[0-9]+)
  LCG_AA_project(CORAL    master     GIT=https://gitlab.cern.ch/lcgcoral/coral.git )
  LCG_AA_project(COOL     master     GIT=https://gitlab.cern.ch/lcgcool/cool.git   )
endif()
LCG_remove_package(Gaudi)
LCG_external_package(pythia8           303            ${MCGENPATH}/pythia8 )
LCG_external_package(madgraph5amc      3.0.3.beta     ${MCGENPATH}/madgraph5amc author=3.0.3.beta)

#---Not supported in Python 2---------------------------------------
LCG_external_package(jsonschema    3.0.1  ) 
LCG_remove_package(jaxlib)
LCG_remove_package(pyhf)
LCG_remove_package(tqdm)
LCG_remove_package(julia)
LCG_remove_package(panel)
LCG_remove_package(fsspec)
LCG_remove_package(dask)
LCG_remove_package(distributed)
LCG_remove_package(mimesis)
LCG_remove_package(bokeh)
LCG_external_package(pillow        6.2.2 ) 
LCG_external_package(tornado       5.1.1 )
LCG_external_package(cloudpickle   1.2.1 )

#---Requires a fix in DD4hep----------------------------------------
LCG_remove_package(acts)

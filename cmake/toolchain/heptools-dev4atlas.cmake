#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    v6-20-00-patches  GIT=http://root.cern.ch/git/root.git )
LCG_external_package(hepmc3  3.2.2  )

# Remove COOL/CORAL for time being (tests, at least, failing)
if(${LCG_COMP} MATCHES clang10)
  LCG_remove_package(COOL)
  LCG_remove_package(CORAL)
endif()


LCG_external_package(fastjet           3.3.2.atlas                            author=3.3.2)
LCG_external_package(evtgen            2.0.0          ${MCGENPATH}/evtgen     tag=R02-00-00 hepmc=3)
LCG_external_package(thepeg            2.2.1          ${MCGENPATH}/thepeg     hepmc=3)
LCG_external_package(herwig3           7.2.1          ${MCGENPATH}/herwig++   thepeg=2.2.1 madgraph=3.0.1.beta openloops=2.1.1 lhapdf=6.2.3 hepmc=3)
LCG_external_package(yoda              1.8.2          ${MCGENPATH}/yoda              )
LCG_external_package(rivet             3.1.1          ${MCGENPATH}/rivet      hepmc=3)
LCG_external_package(pygraphviz        1.5                                       )
LCG_external_package(sherpa            2.2.10          ${MCGENPATH}/sherpa       )
LCG_external_package(sherpa-openmpi    2.2.10.openmpi3 ${MCGENPATH}/sherpa       )
LCG_user_recipe(pygraphviz
  URL ${GenURL}/pygraphviz-${pygraphviz_native_version}.zip
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${PYTHON} setup.py install ${PySetupOptions}  --include-path=${graphviz_home}/include --library-path=${graphviz_home}/lib
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools graphviz
)


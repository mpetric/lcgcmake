#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT     v6.24.00)

#---There are a number of packages not supported with ARM64--------
LCG_remove_package(arrow)
LCG_remove_package(catboost)
LCG_remove_package(cpymad)
LCG_remove_package(llvmlite)
LCG_remove_package(numba)
LCG_remove_package(pyarrow)
LCG_remove_package(pystan)
LCG_remove_package(tensorflow)
LCG_remove_package(tensorboard_plugin_wit)
LCG_remove_package(pyhf)

LCG_remove_package(dcap)
LCG_remove_package(gfal)
LCG_remove_package(srm_ifce)

LCG_remove_package(hydjet)

#---Overwrites of versions ----------(because ARM) ----------------
LCG_external_package(blas      3.8.0.netlib)
LCG_external_package(libtool   2.4.6       )

#---Define the top level packages for this stack-------------------
LCG_top_packages(Geant4 ROOT CMake evtgen pythia8 lcgenv rangev3 cppgsl CppUnit QMtest nose gperftools six networkx ninja xenv
                 RELAX eigen vectorclass lxml rivet fastjet pythia6 crmc starlight AIDA libgit2 ipython fmt
                 COOL CORAL pandas heputils mcutils psutil pygraphviz sqlalchemy requests)

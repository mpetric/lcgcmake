# package updates needed for python 3.9.4 builds

LCG_remove_package(Python)
LCG_external_package(Python 3.9.4)

LCG_remove_package(cffi)
LCG_external_package(cffi              1.14.5                                   )

LCG_remove_package(proj)
LCG_external_package(proj              8.0.0                                    )
LCG_remove_package(pyproj)
LCG_external_package(pyproj            3.0.1                                    )
LCG_remove_package(libgeotiff)
LCG_external_package(libgeotiff        1.6.0                                    )
LCG_remove_package(tiff)
LCG_external_package(tiff              4.2.0                                    )

LCG_remove_package(llvmlite)
LCG_external_package(llvmlite          0.36.0                                   )

LCG_remove_package(typed_ast)
LCG_external_package(typed_ast         1.4.3                                    )

LCG_remove_package(pyzmq)
LCG_external_package(pyzmq             22.0.3                                   )

LCG_remove_package(pystan)
LCG_external_package(pystan            3.0.2                                    )

LCG_remove_package(cryptography)
LCG_external_package(cryptography      3.4.7                                    )

LCG_remove_package(itk)
LCG_remove_package(itk_core)
LCG_remove_package(itk_filtering)
LCG_remove_package(itk_io)
LCG_remove_package(itk_meshtopolydata)
LCG_remove_package(itk_numerics)
LCG_remove_package(itk_registration)
LCG_remove_package(itk_segmentation)
LCG_external_package(itk               5.2.0                                    )
LCG_external_package(itk_core          5.2.0                                    )
LCG_external_package(itk_filtering     5.2.0                                    )
LCG_external_package(itk_io            5.2.0                                    )
LCG_external_package(itk_meshtopolydata 0.7.0                                   )
LCG_external_package(itk_numerics      5.2.0                                    )
LCG_external_package(itk_registration  5.2.0                                    )
LCG_external_package(itk_segmentation  5.2.0                                    )

LCG_remove_package(nbformat)
LCG_external_package(nbformat          5.1.3                                    )

LCG_external_package(networkx          2.5.1                                    )
LCG_external_package(pygraphviz        1.7                                      )
LCG_external_package(theano            1.0.5                                    )

# cartopy is not python3.9 nor PROJ 8 compatible yet
# Python 3.9 is targeted for 0.19.0: https://github.com/SciTools/cartopy/pull/1686
# Proj8: https://github.com/SciTools/cartopy/pull/1752
LCG_remove_package(cartopy)

# tensorflow does not support python 3.9 yet
# https://github.com/tensorflow/tensorflow/issues/44485
if(NOT ${LCG_OS}${LCG_OSVERS} STREQUAL slc6)
  LCG_external_package(tensorflow            2.5.0rc3                             )
endif()
# LCG_remove_package(tensorboard_plugin_wit)
# LCG_remove_package(pyhf)

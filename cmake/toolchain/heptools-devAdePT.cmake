#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix some versions----------------------------------------------
LCG_external_package(cuda       11.1      full=11.1.1_455.32.00)
LCG_external_package(VecGeom    v1.1.11                        )

LCG_top_packages(cuda VecGeom veccore Vc alpaka Boost lcgenv CMake ninja doxygen pytest Geant4)

#---Remove unneeded packages that create problems------------------
LCG_remove_package(tensorflow)
LCG_remove_package(tensorboard_plugin_wit)
LCG_remove_package(torch)
LCG_remove_package(torchvision)
LCG_remove_package(pyhf)

#---We don't need ROOT in dev but without a defined version CMake fails
LCG_external_package(ROOT v6.22.06)
LCG_external_package(cudnn 7.6.5.32)

#---We neeed Geant4 built without VecGeom, so we chnage the recipe--
LCG_user_recipe(Geant4
    URL ${GenURL}/geant4.<Geant4_native_version>.tar.gz
    CMAKE_ARGS  -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
                -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
                -DGEANT4_BUILD_CXXSTD=<CMAKE_CXX_STANDARD>
                -DGEANT4_USE_GDML=ON
                -DXERCESC_ROOT_DIR=<XercesC_home>
                -DGEANT4_USE_SYSTEM_CLHEP=ON
                -DGEANT4_USE_G3TOG4=ON
                -DGEANT4_INSTALL_DATADIR=<Geant4_datadir>
                -DGEANT4_BUILD_TLS_MODEL=global-dynamic
                -DGEANT4_USE_SYSTEM_EXPAT=ON
                -DGEANT4_INSTALL_PACKAGE_CACHE=OFF
                -DGEANT4_USE_USOLIDS=OFF
    BUILD_COMMAND $(MAKE)
    DEPENDS     XercesC expat motif clhep
)
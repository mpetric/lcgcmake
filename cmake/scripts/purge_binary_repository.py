#!/usr/bin/env python
"""
Program to install a complete set of LCG binary tarfiles.
<pere.mato@cern.ch>
Version 1.0
"""
#-------------------------------------------------------------------------------
from __future__ import print_function
import os, sys, glob, time
try:
    import argparse
except ImportError:
    import argparse2 as argparse

def creation_date(path_to_file):
  stat = os.stat(path_to_file)
  try:
    return stat.st_birthtime
  except AttributeError:
    return stat.st_mtime

def purge_repository(purgedir, platforms, days=7, releasedir=None):
  #---Change dir to the directory to be purged-----------------------------------
  os.chdir(purgedir)

  if not platforms:
    #---Get the list of platforms from the existing summary files----------------
    summaries = glob.glob('summary-*.txt')
    for summary in summaries:
      platforms.append(os.path.splitext(summary)[0].replace('summary-',''))

  #---Loop over all platforms----------------------------------------------------
  for platform in platforms:
    #---Process files of the form LCG_<version>_<platform>.txt to get the needed tarfiles
    p = platform.split('-')
    textfiles = glob.glob('LCG_*_%s.txt' % (p[0]+'*-'+'-'.join(p[1:])))
    print('Getting the needed tarfiles from these files: ', textfiles)
    needed_tarfiles = set()
    for textfile in textfiles :
      with open(textfile, 'r') as f:
        lines = [x.strip() for x in f.readlines()]
        for line in lines:
          if line[0] == '#' : continue
          d = {}
          for key, value in [x.split(':',1) for x in line.split(', ')]:
            d.update({key.strip(): value.strip()})
          needed_tarfiles.add(d['NAME']+'-'+d['VERSION']+'_'+d['HASH']+'-'+d.get('PLATFORM', platform)+'.tgz')
    #---Get the files in directory------------------------------------------------
    curr_tarfiles = set(glob.glob('*-%s.tgz' % platform))
    #---Delete unneeded files------------------------------------------------------
    for tarfile in curr_tarfiles-needed_tarfiles:
      if (time.time() - creation_date(tarfile)) > days*24*3600 : 
        print('Deleting ... ', tarfile)
        os.remove(tarfile)
    #---Delete already existing tarfiles in releases------------------------------
    if releasedir:
      curr_tarfiles = set(glob.glob('*-%s.tgz' % platform))
      for tarfile in curr_tarfiles:
        if os.path.exists(os.path.join(releasedir, tarfile)):
          print('Deleting (already in releases) ... ', tarfile)
          os.remove(tarfile)
    #---Create new summary file---------------------------------------------------
    print('Creating summary file ...', 'summary-'+platform+'.txt')
    with open('summary-'+platform+'.txt', 'w') as f:
      for tarfile in glob.glob('*-%s.tgz' % platform) : f.write(tarfile+'\n')

#---Main program----------------------------------------------------------------
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('platforms', metavar='PLATFORMS', nargs='*', default=[], help='list of platforms to purge')
  parser.add_argument('-C', dest='purgedir', default=os.getcwd(), help='directory to be purged')
  parser.add_argument('--releasedir', dest='releasedir', default=None, help='directory with released tarfiles')
  parser.add_argument('--days', dest='days', type=int, default=7, help='purge only files older than this number of days')
  args = parser.parse_args()

  purge_repository(args.purgedir, args.platforms, args.days, args.releasedir)
